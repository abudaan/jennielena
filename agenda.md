---
layout: default
title: Agenda Jennie Lena
class_name: agenda
permalink: /agenda/
agenda:
  2015:
    - Mundial festival: 9 oktober 2015
    - Dauphine, Amsterdam: 12 augustus 2015
    - Appelpop, Tiel (supporting vocals bij Anouk): 11 september 2015
    - Stoppelheane, Raalte (supporting vocals bij Anouk): 27 augustus 2015
    - ZandFestival, Almere (supporting vocals bij Anouk): 22 augustus 2015
    - GlemmerRock, Strand Lemmer (supporting vocals bij Anouk): 20 augustus 2015
    - Suikerrock, Tienen België (supporting vocals bij Anouk): 31 juli 2015
    - Wijk aan Zee (besloten): 11 juli 2015
    - Rock Werchter, België (supporting vocals bij Anouk): 4 juli 2015
    - Besloten, Amsterdam: 29 juni 2015
    - Volvo Ocean Race, Scheveningen (supporting vocals bij Anouk): 20 juni 2015
    - Gaypride event, Utrecht: 19 juni 2015
    - BrandedU event, Amsterdam: 15 juni 2015
    - Pinkpop, Landgraaf (supporting vocals bij Anouk): 13 juni 2015
    - Vestrock, Hulst (supporting vocals bij Anouk): 6 juni 2015
    - Indian Summer, Langedijk (supporting vocals bij Anouk): 5 juni 2015
    - TEDxAmsterdamWomen, Amsterdam: 29 mei 2015
    - Masterclass @ Conservatorium, Enschede: 22 april 2015
    - Workshop Performing Art: 10, 11, 12 april 2015
    - Paaspop, Schijndel (supporting vocals bij Anouk): 4 april 2015
    - SportPaleis, België (supporting vocals bij Anouk): 7 maart 2015
    - Jenny Lane, besloten, Eindhoven: 16 maart 2015
    - Jenny Lane, besloten, Eindhoven: 10 januari 2015
    - Splendor Parade, Muziekgebouw aan het IJ, Amsterdam: 3 januari 2015
  2014:
    - 40Up, Paradiso, Amsterdam: 29 december 2014
    - Masterclass, Amsterdam: 16 december 2015
    - Masterclass, Amsterdam: 15 december 2014
    - Studio 24, Hilversum: 12 december 2014
    - Dauphine, (besloten), Amsterdam: 10 december 2014
    - NBC (besloten), Nieuwegein: 9 december 2014
    - Monaco, France: 6 december 2014
    - Thopss, Amsterdam: 28 november 2014
    - Club Dauphine, Amsterdam: 18 november 2014
    - Workshop, Masterclass, Arnhem: 13 november 2014
    - Workshop, Masterclass, Soest: 10 november 2014
    - Teats, Zaandam: 6 november 2014
    - Rotown Rotterdam: 4 oktober 2014
    - Rotown Rotterdam: 4 oktober 2014
    - Besloten, Rotterdam: 27 september 2014
    - Muziekgebouw aan het IJ, Amsterdam: 20 september 2014
    - Hotel Droog, Amsterdam: 8 september 2014
    - Jazz at the Lake, Leiden: 16 augustus 2014
    - Besloten, Zeeland: 3 augustus 2014
    - Besloten, Tilburg: 27 juni 2014
    - Club Dauphine, Amsterdam: 20 juni 2014
    - Vondelpark, Amsterdam: 14 juni 2014
    - Hotel Arena, Amsterdam: 5 juni 2014
    - Parkfeest, Oosterhout: 26 mei 2014
    - Besloten, Amsterdam: 15 mei 2014
    - Soul Live, Paard van Troje, Den Haag: 12 april 2014
    - Besloten, Amsterdam: 27 maart 2014
    - Soul Live, De Vorstin, Hilversum: 8 maart 2014
    - Ladies of Zoul, ZiggoDome, Amsterdam: 15 februari 2014
    - Ladies of Zoul, ZiggoDome, Amsterdam: 14 februari 2014
    - Panama Festival, Amsterdam: 11 februari 2014
    - Plukje Geluk, Amsterdam: 4 februari 2014
    - North Sea Jazz Club, Amsterdam: 31 januari 2014
    - North Sea Jazz Club, Amsterdam: 31 januari 2014

---

<div class="agenda">

  <p>
    Wil je op de hoogte worden gehouden van Jennie’s Tourlijst?
    <br>Abonneer je op de <a href="../newsletter">nieuwsbrief!</a>
  </p>


  {% for year in page.agenda %}
    <h2>{{ year[0] }}</h2>
    {% for items in year[1] %}
      {% for item in items %}
        <div class="item">
          <span>{{item[0]}}</span><br>
          <span>{{item[1]}}</span>
        </div>
      {% endfor %}
    {% endfor %}
  {% endfor %}

</div>


