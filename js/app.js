
global.jQuery = require('jquery');
require('jquery.easing');
let $ = global.jQuery;

window.onload = function(){

  let mobile_button = document.querySelectorAll('#navigation-mobile>.header>.icon')[0];
  let mobile_menu_items = document.querySelectorAll('#navigation-mobile .items')[0];

  let collapsed = false;

  mobile_button.addEventListener('click', function(){
    collapsed = !collapsed;
    mobile_menu_items.style.top = collapsed ? '0' : '-100vh';
  }, false);

  let scroll_button = document.getElementById('scroll-to-top');
  document.addEventListener('scroll', function(){
    scroll_button.style.display = document.body.scrollTop > 150 ? 'block' : 'none';
  }, false);

  //scroll_button.style.display = document.body.scrollTop > 0 ? 'block' : 'none';

  scroll_button.addEventListener('click', function(e){
    e.preventDefault();
    e.stopImmediatePropagation();
    $('body').animate({scrollTop: '0px'}, {easing: 'easeOutQuint', duration: 900});
  }, false);

  let url = "https://graph.facebook.com/v2.5/245872895455994/posts?access_token=191506317857257%7C0bd3bac7c146d5adc10c217552950ed9&debug=all&format=json&method=get&pretty=0&suppress_http_code=1&fields=attachments";
  let url2 = "https://graph.facebook.com/245872895455994/posts?access_token=191506317857257|0bd3bac7c146d5adc10c217552950ed9&fields=attachments&limit=10";
  $.ajax({
    url: url
  }).done(function(data){
    console.log(data);
  });
};
